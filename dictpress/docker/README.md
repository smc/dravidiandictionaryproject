# Docker

`docker-compose.yml` is the starting point for docker based deployment.

Here, we have caddy as the server (taking care of automatic TLS, etc.), glossary and dictpress as two applications, and postgres image as database.

We need to create data folders for docker to mount.

```
mkdir -p data/caddy_{config,data} data/db_data
```

The `dictpress.csv` file must be present in dictpress folder (as sibling of convert.py, config.toml, etc.). This can be generated by putting `unicode.csv` in the root of this repository (that's as a sibling of dictpress, index.php, fix.js, etc.) and then running `python convert.py`.

Then we can build the images and run them with 

```
docker compose up -d
```

This will do the following:

* Create a postgres database with default credentials
* Compile dictpress with the config.toml in the dictpress folder
* Compile glossary app
* [Only on first run] install dictpress in db
* [Only on first run] load dictpress.csv into dictpress
* Run caddy with Caddyfile from dictpress/docker/Caddyfile


Presently, the Caddyfile is configured to run on localhost:4444 which is where you have to go to see the running dictionary.

**Please note** that importing the CSV will take a long time and things might appear empty for a while. (Look for log lines like `importer.go:137: imported 5000 entries and 15000 definitions`)

**Also note** that using this docker-compose comes with no warranty that it is future compatible. Work as if we might have to destroy everything and start from scratch.