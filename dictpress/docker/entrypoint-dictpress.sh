#!/bin/sh
cd /app
if [ -e NEEDS_INSTALL ]; then
    echo "First run of dictpress. Initializing and loading data"
    rm NEEDS_INSTALL
    yes | ./dictpress --install
    ./dictpress --import=dictpress.csv &
fi
./dictpress --site=./site