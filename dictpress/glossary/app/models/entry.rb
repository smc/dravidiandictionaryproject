class Entry < ApplicationRecord
  LANGS = %i[malayalam kannada tamil telugu]

  has_many :relations, foreign_key: :from_id
  has_many :definitions, through: :relations, source: :to_entry

  enum lang: Hash[LANGS.map { |symbol| [symbol, symbol.to_s] }]

  LANGS.each do |target_lang|
    define_method :"#{target_lang}_defintions" do
      return self if lang.to_sym == target_lang

      definitions.where(lang: target_lang).select(%i[content]).first
    end
  end
end
