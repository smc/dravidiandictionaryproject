#!/bin/bash

kill $(ps aux | grep '[g]lossary' | awk '{print $2}') || true
scriptDir=$(dirname "$(readlink -f "$0")")
cd $scriptDir
source glossary.env
cd glossary
nohup bundle exec rails s &> run.log &
