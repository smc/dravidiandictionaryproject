# Setup

Let's start with pure debian bookworm


### Packages

First we will install the required packages

```bash
apt install ruby golang postgresql git ufw make tmux caddy bundler libyaml-dev libpq-dev
```

These are required for the following reasons
* ruby and bundler for glossary app
* libyaml-dev because ruby wants to build psych which requires yaml.h
* libpq-dev because ruby wants to build pg
* caddy for server
* tmux for devops convenience
* make for dictpress building
* ufw for security
* git for cloning this repo
* postgresql is the database for dictpress
* golang to build dictpress


### UFW

You can [set up ufw as per digital ocean guide](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-debian-11)

### Caddy

We could already set up caddy. 

```bash
$ cat /etc/caddy/Caddyfile 
samam.net {
    reverse_proxy localhost:8081
    reverse_proxy /glossary* localhost:3000
}
```

This makes everything go to dictpress (listening at 8081) and everything under glossary go to the glossary rails app. (We will set these up soon.)


### Postgresql

After having installed postgres above, we need to create a role and a database. For this we can use the `createuser` and `createdb` commands. But these need to be run as postgres user. `-P` will allow us to set password (which is what we will use in dictpress and glossary to connect to db). `-O` will set the ownership of the database to the user we created.

```bash
su - postgres
[postgres] createuser -P dictionary
[postgres] createdb -O dictionary dictionary
```

### User 

Now we can set up a user

```bash
adduser dictionary
loginctl enable-linger dictionary
```

(linger is required to run systemd services as user)

### Applications

From now we can run everything by logging in as the new dictionary user.

Now, let us get samam repo on the 

```bash
git clone --recursive https://gitlab.com/smc/samam.git
```

The recursive flag allows you to omit this:

```bash
git submodule init
git submodule update
```

### Dictpress

First build dictpress including its static dependencies.

```bash
cd ~/samam/dictpress/dictpress-repo
make dist
```

Let's have a separate folder which we use to deploy dictpress

```bash
mkdir ~/dictpress
cd ~/dictpress
ln -s ../samam/dictpress/dictpress-repo/dictpress .
cp ../samam/dictpress/config.toml .
```

We can edit `config.toml` to include the database details from above and to change the admin password, etc.

```bash
./dictpress --install
```

This will wipe the database and install the thing.

To run the server, we need a systemd user unit

```bash
vim ~/.config/systemd/user/dictpress.service
cat ~/.config/systemd/user/dictpress.service
[Unit]
Description=dictpress

[Service]
WorkingDirectory=/home/dictionary/dictpress
ExecStart=/home/dictionary/dictpress/dictpress --site=/home/dictionary/samam/dictpress/site

[Install]
WantedBy=default.target
```

We can enable and start this

```bash
systemctl --user daemon-reload
systemctl --user enable --now dictpress
```

daemon-reload is required every time you edit the systemd unit.

Use the following if you need to see status, etc.

```bash
systemctl --user status dictpress
journalctl --user -u dictpress
```


### Initial data

We need to get the dictpress.csv on to the server. From our local system we can do:

```bash
scp dictpress.csv dictionary@samam.net:
```

Then we come back to the server and import this

```bash
cd dictpress
./dictpress --import=../dictpress.csv
```

### glossary

We will avoid using sudo by installing things in ~/.gem

```bash
bundle config set --local path /home/dictionary/.gem 
cd ~/samam/dictpress/glossary
bundle install
```

To get the binaries installed by this we will need to edit `~/.profile` and add these:

```bash
if [ -d "$HOME/.gem/ruby/3.1.0/bin" ] ; then
    PATH="$HOME/.gem/ruby/3.1.0/bin:$PATH"
fi
if [ -d "$HOME/go/bin" ] ; then
    PATH="$HOME/go/bin:$PATH"
fi
```

(This includes the binary path of go as well)

Now, let us precompile the assets

```bash
cd ~/samam/dictpress/glossary
rails assets:precompile
```

And set up the systemd user unit

```bash
vim ~/.config/systemd/user/glossary.service
cat ~/.config/systemd/user/glossary.service
[Unit]
Description=glossary

[Service]
WorkingDirectory=/home/dictionary/samam/dictpress/glossary
Environment=RAILS_ENV=production
Environment=RAILS_SERVE_STATIC_FILES=1
ExecStart=/home/dictionary/.gem/ruby/3.1.0/bin/rails s

[Install]
WantedBy=default.target
```

Don't forget to start it

```bash
systemctl --user daemon-reload
systemctl --user enable --now glossary
```
