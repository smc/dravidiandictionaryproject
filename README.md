# Samam

Njatyela Shreedharan (ഞാറ്റ്യേല ശ്രീധരൻ) build a dictionary in 4-languages (ചതുർ ഭാഷ നിഘണ്ടു) after working on it for 25 years.
His work is very well presented in a one hour documentary [Dreaming of Words](https://youtu.be/IWmRnOcm_Ko?si=BZyGnx9XvCkU6mDQ). It won the National Film Award for Best Educational/Motivational/Instructional Film (2020).

[![Dreaming of Words (with English subtitles) | National Award Winning Documentary](https://img.youtube.com/vi/IWmRnOcm_Ko/default.jpg)](https://youtu.be/IWmRnOcm_Ko)

## How to use Samam

Use the website [samam.net](https://samam.net/) to search for words, or browse through the glossary.
