## Usage

```bash
php -S localhost:3000
```

* http://localhost:3000/1-original/index.php
* http://localhost:3000/2-better-tables/index.php
* http://localhost:3000/3-csv/index.php

## Edge cases

* മ്ല is not mapped. The character is "¾"

## Entry problems

* `AZrjvSw þ ImWm³ Ign-bm-¯Xv` (അദൃഷ്ടം - കാണാന്‍ കഴിയാത്തത്) has no definitions
* There are some English words in the original file, these get converted to Unicode when passed to Freakenz

`<B>C&#169;n&#179;</B> &thorn; engine` -> `ഇഞ്ചിന്‍ - ലിഴശില` ->should be-> `ഇഞ്ചിന്‍ - Engine`
`\ss¯(Snail)`