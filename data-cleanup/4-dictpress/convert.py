import csv
import json
import re

from libindic.payyans import Payyans
from libindic.transliteration import getInstance

payyans = Payyans()
t = getInstance()

def makeRow(row, lang, lang_expanded):
  columnIndex = 1
  if lang == "kn":
    columnIndex = 3
  elif lang == "ta":
    columnIndex = 5
  elif lang == "te":
    columnIndex = 7

  word = row[columnIndex]
  definitions = row[columnIndex+1]

  first_letter = word[0]
  native_word = t.transliterate(word, f"{lang}_IN").strip()

  definitions_native = ""
  definitions_en = ""
  definitions_ipa = ""

  word_buffer = ""
  index = 0
  for part in definitions:
    last_character = index == len(definitions) - 1
    if part in [" ", ",", ".", "(", ")", "-"] or last_character:
      after = part
      if last_character:
        word_buffer += part
        after = ""

      definitions_native += t.transliterate(word_buffer, f"{lang}_IN").strip() + after
      definitions_en += t.transliterate(word_buffer, f"en_US").strip() + after
      definitions_ipa += t.transliterate(word_buffer, f"IPA").strip() + after
      word_buffer = ""
    else:
      word_buffer += part
    index += 1

  meta = {
    "word_native": native_word,
    "word_en": t.transliterate(native_word, "en_US").strip(),
    "word_ipa": t.transliterate(native_word, "IPA").strip(),

    "definitions": definitions,
    "definitions_native": definitions_native,
    "definitions_en": definitions_en,
    "definitions_ipa": definitions_ipa
  }

  return [
    "-" if lang == "ml" else "^",
    first_letter,
    word,
    lang_expanded,
    "", "", "",
    row[0],
    "", "",
    json.dumps(meta, ensure_ascii=False)
  ]

faulty = []
rowsToWrite = []

with open('../3-csv/unicode.csv', 'r') as file:
  reader = csv.reader(file, delimiter=' ')

  for row in reader:
    if row[1] == "":
      continue
    
    if row[1] == "" or row[3] == "" or row[5] == "":
      faulty.append(row)
      continue

    rowsToWrite.append(makeRow(row, "ml", "malayalam"))
    rowsToWrite.append(makeRow(row, "kn", "kannada"))
    rowsToWrite.append(makeRow(row, "ta", "tamil"))
    rowsToWrite.append(makeRow(row, "te", "telugu"))

  print(len(faulty), reader.line_num)
  for item in faulty:
    print(item)
        
with open('dictpress.csv', mode='w') as file:
  writer = csv.writer(file, delimiter=',', quotechar='"')

  for row in rowsToWrite:
    writer.writerow(row)