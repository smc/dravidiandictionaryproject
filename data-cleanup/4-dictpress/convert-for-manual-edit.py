import csv
import json

rowsToWrite = [
  [
    "ID", "Language", "Word in malayalam", "Word natively (use English for Malayalam entry)", "Word transliteration", "Word IPA",
    "Defs in malayalam", "Defs natively (use English for Malayalam entry)", "Defs transliteration", "Defs IPA"
  ]
]

def makeRow(row):
  lang = row[3]
  meta = json.loads(row[10])

  newRow = [
    row[7] if lang == "malayalam" else "", # ID
    lang, # Language
    row[2], # Word
    "" if lang == "malayalam" else meta["word_native"],
    meta["word_en"],
    meta["word_ipa"],
    meta["definitions"],
    "" if lang == "malayalam" else meta["definitions_native"],
    meta["definitions_en"],
    meta["definitions_ipa"]
  ]
  return newRow

with open('./dictpress.csv', 'r') as file:
  reader = csv.reader(file, delimiter=',')

  for row in reader:
    rowsToWrite.append(makeRow(row))
        
with open('100-entries.csv', mode='w') as file:
  writer = csv.writer(file, delimiter=',', quotechar='"')

  for row in rowsToWrite:
    writer.writerow(row)
