$("font[color='#ffffff']").remove()

const fontStyle = `
<style type="text/css">
@font-face {
  font-family: 'ML-TTKarthika';
  src: url('/ML-TTKarthikaNormal.woff2') format('woff2'),
      url('/ML-TTKarthikaNormal.woff') format('woff');
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}
body {
  font-family: ML-TTKarthika;
}
</style>`

$("body").append(fontStyle)

jQuery.fn.outerHTML = function() {
  return (this[0]) ? this[0].outerHTML : '';  
};

const partialFirstRowTables = []

function getColumns(table) {
  lastTd = null
  cols = []
  $(table).find("td[valign='TOP']").each(function() {
    if ($(this).find("p").length < 10) return;
    cols.push(this)
  })
  cols.sort(function(a, b) { 
    return $(a).offset().left - $(b).offset().left;
  })

  const firstRowHasText = $(cols[0]).find("p").first().text().trim() !== "" ||
    $(cols[1]).find("p").first().text().trim() !== "" ||
    $(cols[2]).find("p").first().text().trim() !== "" ||
    $(cols[3]).find("p").first().text().trim() !== ""

  const partialFirstRow = $(cols[0]).find("p").first().find("b").text().length < 2 &&
    $(cols[1]).find("p").first().find("b").text().length < 2 &&
    firstRowHasText

  if (partialFirstRow) {
    partialFirstRowTables.push($(table).attr("page"))
  }
  return cols
}

async function outputHtml(innerBody) {
  const tableStyle = `
  <style>
  td {
    border: 1px solid #000;
    padding: 10px 15px;
    width: 25%;
    vertical-align: top;
  }
  p {
    border: 1px solid #000;
  }
  </style>
  `
  const html = `<html><head></head><body>${innerBody}${fontStyle}${tableStyle}</body></html>`

  const formattedHtml = await prettier.format(html, { parser: 'html', plugins: prettierPlugins });

  var formData = new FormData();
  formData.append('file', new File([new Blob([formattedHtml])], "file.html"));

  $.ajax({
    url: 'process.php',
    data: formData,
    processData: false,
    contentType: false,
    type: 'POST'
  });
}

function fixTable(table) {
  const refinedTable = $("<table>")
  refinedTable.attr("page", $(table).attr("page"))

  const tbody = $("<tbody>")
  $(`<div style="font-size: 50px;font-weight: bold;text-align: center;">${$(table).attr("page")}</div>`)
    .appendTo(refinedTable)
  tbody.appendTo(refinedTable)

  const columns = getColumns(table)
  const rowsInColumns = [
    $(columns[0]).find("p").length,
    $(columns[1]).find("p").length,
    $(columns[2]).find("p").length,
    $(columns[3]).find("p").length
  ]

  const tr = $("<tr>")
  for (let i = 0; i <= 3; i++) {
    const td = $("<td>")
    $(columns[i]).find("p").each(function() {
      $(this).appendTo(td);
    })
    td.appendTo(tr)
  }
  tr.appendTo(refinedTable)

  return refinedTable
}

function doForAllTables() {
  const refinedTables = []
  $("table").each(function() {
    refinedTables.push(fixTable(this))
  })
  outputHtml(refinedTables.map(t => t.outerHTML()).join(""))
}

$("table").each(function() {
  const pageNumber = this.previousSibling.previousSibling.textContent.match(/page\s(.*?)\s/)[1]
  $(this).attr("page", pageNumber)
})

doForAllTables()
// outputHtml(fixTable($("table[page='1']")).outerHTML())