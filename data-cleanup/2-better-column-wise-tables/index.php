<?php
if (isset($_POST["process"])) {
  for ($i = 1; $i <= 9; $i++) {
    include "../1-original/$i.htm";
  }
?>
  <script src="https://unpkg.com/prettier@3.0.3/standalone.js"></script>
  <script src="https://unpkg.com/prettier@3.0.3/plugins/html.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="fix-tables.js"></script>
<?php
} else {
?>
  This step is a destructive process, the HTML output from this step <b>source-of-truth.html</b> is the source of truth which is edited manually and tracked in git. So doing this step will overwrite the file. Hence the "destructive" process.

  Are you sure you want to do this step?

  <br/><br/>
  <form method="POST">
    <button name="process">Yes</button>
  </form>
<?php
}
?>