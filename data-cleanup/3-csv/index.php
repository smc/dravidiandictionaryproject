TODO before CSV, make the table row-wise so it's easier to analyze missing rows

<button type="button" onclick="makeCsvForAllTables()">Make CSV</button>

<textarea id="output" cols="200" rows="10"></textarea>

<?php
// Configure how mant tables to show
$number_of_tables = 100;

$src = file_get_contents("../2-better-column-wise-tables/source-of-truth.html");
$table_count = 0;

$tables = preg_split('/\<\/table\>+/', $src);

for ($i = 0; $i < $number_of_tables; $i++) {
  echo $tables[$i];
}
echo "</table>"
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="fix-tables.js"></script>
<script src="make-csv.js"></script>

<style>
  @font-face {
    font-family: 'ML-TTKarthika';
    src: url('/ML-TTKarthikaNormal.woff2') format('woff2'),
        url('/ML-TTKarthikaNormal.woff') format('woff');
    font-weight: normal;
    font-style: normal;
    font-display: swap;
  }
  body, textarea {
    font-family: ML-TTKarthika;
  }
  tr td {
    width: 25%;
    border: 1px solid #000;
    vertical-align: top;
    padding: 10px;
  }
  csvrow:nth-child(1) {
    border-right: 2px solid #000;
    padding-right: 5px;
    margin-right: 5px;
    font-weight: bold;
  }
</style>