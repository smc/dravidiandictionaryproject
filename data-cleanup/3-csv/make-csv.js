(function(window) {
  function getRows(table) {
    const rows = []
    $(table).find("tr").each(function() {
      const row = []
      $(this).find("csvrow").each(function() {
        row.push($(this).fxText())
      })
      rows.push(row)
    })
    return rows
  }

  function pagemakerTextCleanup(text) {
    return text
      .replace(/\n/g, "")
      .replace("&quot;", "'")
      .replace("¾", "മ്ല") // This is missed out in Freakenz
  }

  function convertToCsv(rows) {
    var makeRow = function(row) {
      return row.map(elem => 
        `"${pagemakerTextCleanup(elem.toString()).replace(/"/g, '""')}"`
      ).join(" ");
    }
    return rows.map(row => makeRow(row)).join("\n");
  }

  function makeOutput(text) { 
    $("#output").val(text)
  }

  window.makeCsvForAllTables = function() {
    const rows = []
    $("table").each(function() {
      const tableRows = getRows(this)

      if (partialFirstRowTables.includes($(this).attr("page"))) {
        for (let i = 1; i <= 7; i+=2) {
          rows[rows.length - 1][i] += tableRows[0][i]
        }
        rows.push(...tableRows.slice(1))
      } else {
        rows.push(...tableRows)
      }
    })

    console.log(partialFirstRowTables)
    makeOutput(convertToCsv(rows))
  }

  // makeOutput(convertToCsv(getRows($("table[page='5']").last())))
})(window);