jQuery.fn.outerHTML = function() {
  return (this[0]) ? this[0].outerHTML : '';  
};

jQuery.fn.fxText = function() {
  return (this[0]) ? this.text().trim().replace(/\s\s+/g, ' ') : '';  
};

const partialFirstRowTables = []
const nonEqualColumnTables = []

function getColumns(table) {
  lastTd = null
  cols = []
  $(table).find("td").each(function() {
    if ($(this).find("p").length < 10) return;
    cols.push(this)
  })
  cols.sort(function(a, b) { 
    return $(a).offset().left - $(b).offset().left;
  })

  const firstRowHasText = $(cols[0]).find("p").first().fxText() !== "" ||
    $(cols[1]).find("p").first().fxText() !== "" ||
    $(cols[2]).find("p").first().fxText() !== "" ||
    $(cols[3]).find("p").first().fxText() !== ""

  const partialFirstRow = $(cols[0]).find("p").first().find("b").text().length < 2 &&
    $(cols[1]).find("p").first().find("b").text().length < 2 &&
    firstRowHasText

  if (partialFirstRow) {
    partialFirstRowTables.push($(table).attr("page"))
  }
  return cols
}

function fixTable(table) {
  const refinedTable = $("<table>")
  const pageNumber = $(table).attr("page")
  refinedTable.attr("page", pageNumber)

  const tbody = $("<tbody>")
  tbody.appendTo(refinedTable)

  const cols = getColumns(table)

  const colCursors = [0, 0, 0, 0]

  const getColumnValue = (colIndex) => {
    let pElem = $(cols[colIndex]).find("p").eq(colCursors[colIndex]),
        text = pElem.fxText();

    const firstColumnText = $(cols[0]).find("p").eq(colCursors[0]).fxText()

    // ImWpI = കാണുക
    // Some entries are like this: അവമാനം - 'അപമാനം' കാണുക
    // with other columns empty values
    const isKaanukaEntry = firstColumnText.includes("ImWpI")

    const firstRowOfTableIsContinuationOfLastTable = partialFirstRowTables.includes(
      pageNumber
    )

    if (isKaanukaEntry || firstRowOfTableIsContinuationOfLastTable) {
      return { text, pElem }
    }

    // console.log(colCursors, colIndex, text)

    while (
      (pElem = $(cols[colIndex]).find("p").eq(colCursors[colIndex])).length !== 0 &&
      (text = pElem.fxText()) === "" &&
      (pElem.attr("include") === undefined)
    ) {
      colCursors[colIndex]++
    }

    return { text, pElem }
  }

  const addCell = (tr, pElem) => {
    const td = $("<td>")
    td.appendTo(tr)

    const b = pElem.find("b:first")
    wordText = b.fxText().replace(/\sþ$/, "")
    $("<csvrow>").html(wordText).appendTo(td)
    b.remove()

    const defintionText = pElem.fxText()
      .replace(/^\([1-9]\)\s/, "")
      .replace(/^þ\s/, "") // þ = -
    $("<csvrow>").html(defintionText).appendTo(td)
  }

  let pElem, rowNumber = 1;
  while ((pElem = $(cols[0]).find("p").eq(colCursors[0])).length !== 0) {
    const firstColumn = getColumnValue(0)
    const text = firstColumn.text

    // There are letter heading entries. Example: see above "CI-gv¯pI" (ഇകഴ്ത്തുക)
    const isLetterHeading = text.length >= 1 && text.length <= 2

    if (isLetterHeading) {
      colCursors[0]++
      continue
    }

    const tr = $("<tr>")
    $("<csvrow style='display: table-cell;vertical-align: middle;'>").text(`${pageNumber}.${rowNumber}`).appendTo(tr)
    addCell(tr, firstColumn.pElem)
    for (let i = 1; i <= 3; i++) {
      addCell(tr, getColumnValue(i).pElem)
    }
    tr.appendTo(tbody)

    colCursors[0]++
    colCursors[1]++
    colCursors[2]++
    colCursors[3]++

    rowNumber++
  }

  if (
    (colCursors[0] + colCursors[1] + colCursors[2] + colCursors[3]) % colCursors[0] !== 0 &&
    $(table).attr("verified") === undefined
  ) {
    nonEqualColumnTables.push(pageNumber + "-" + colCursors.join(","))
  }

  return refinedTable
}

function fixAllTables() {
  $("table").each(function() {
    $(this).replaceWith(fixTable(this))
  })
  console.log("Partial tables", partialFirstRowTables)
  console.log("Non equal column tables", nonEqualColumnTables)
}

fixAllTables()
